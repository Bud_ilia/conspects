## Федеративное обучение

### Концепты и базовые идеи

 Данные не всегда хранятся централизованно по следующим причинам:

1. **приватность данных.** Не все данные можно пересылать из одного места в другое. Например, ввиду юридических причин;
2. **важность данных.**. Люди не всегда хотят делится данными, так как считают, что в данных есть потенциал;
3. **большой объем данных.** И настолько много, что нельзя собрать их в одно место.

Можно сравнить 2 альтернативны обучения.
1. централизованное - данные собираются в одно место и там же происходит обучение на одном куске данных.
2. федеративное/коаперативное - для каждой части данных обучается своя модель. Т. е. нужно модели посылать к данным, которые могут находиться в разных местах. Необходимы **локальные сервера**, обучающие модель и посылающие на **агрегационный сервер**, который агрегируюет полученные модели и отсылающий агрегированную модель обратно на **локальные сервера**.

Стратегии агрегации моделей:
1. Усреднение весов полученной модели, пропорционально объему датасета на каждом устройстве. (14:24 на видео)

#### Пример федеративного обучения
К примеру есть 3 больницы, где данные нельзя пересылать в одно место. Можно завести агрегационный сервер, который будет агрегировать модели. Но чтобы была возможность агрегировать модели, нужно получать на вход модели.

Каждая больныца обучает на своем куске данных свою модель, затем посылает **updated model**. Агрегационный сервер эти модели агрегирует, создавая на основе их свою модель, которая посылается обратно в на больничные сервера. И обучение продолжается.

### Полезные ссылки

1. [Federated Learning: Collaborative Machine Learning without Centralized Training Data](https://ai.googleblog.com/2017/04/federated-learning-collaborative.html)


Остановился на 15:00 на видео.